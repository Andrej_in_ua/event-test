<?php


use App\Service\Event\AbstractEvent;
use App\Service\Event\EventDispatcher;
use App\Service\Event\EventInterface;

class TestEvent extends AbstractEvent {
}

class EventDispatcherTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @throws ReflectionException
     */
    public function testFire(): void
    {
        $connectionMock = $this->createMock(\App\Service\Database\Connection::class);
        $service = new EventDispatcher($connectionMock);

        $testEvent = new TestEvent();

        $service->registerListener($testEvent::getEventName(), function(EventInterface $event) use ($testEvent) {
            $this->assertEquals($testEvent, $event, 'Invalid event object send to listener callback');
        });

        $stmMock = $this->createMock(\PDOStatement::class);

        $attributes = [];
        $stmMock->expects($this->atLeast(5))->method('bindValue')->willReturnCallback(function(string $attr, $value) use (&$attributes) {
            $attributes[] = $attr;
            switch ($attr) {
                case ':eventName':
                    $this->assertEquals('TestEvent', $value);
                    break;

                case ':ip':
                    $this->assertEquals('127.0.0.1', $value);
                    break;

                case ':userAgent':
                    $this->assertNull($value);
                    break;

                case ':event':
                    $event = unserialize($value);
                    $this->assertInstanceOf(TestEvent::class, $event);
                    break;
            }
        });

        $connectionMock->expects($this->once())->method('prepare')->willReturn($stmMock);

        $service->fire($testEvent);

        // Это очень плохая проверка и в реальных проектах её нельзя использовать
        // Тут это сдално только из-за того, что нет прослойки для работы с DB
        $this->assertEmpty(array_diff([':eventName', ':ip', ':userAgent', ':event', ':created'], $attributes));
    }
}