<?php


namespace App\Service\Author\Event;


use App\Service\Event\AbstractEvent;

/**
 * Class AuthorRequestRoyaltyEvent
 *
 * @package App\Service\Author\Event
 */
class AuthorRequestRoyaltyEvent extends AbstractEvent
{
    /**
     * @var int
     */
    private $authorId;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $paymentDetails;

    /**
     * AuthorRequestRoyaltyEvent constructor.
     *
     * @param int $authorId
     * @param string $amount
     * @param string $currency
     * @param string $paymentDetails
     */
    public function __construct(int $authorId, string $amount, string $currency, string $paymentDetails)
    {
        $this->authorId = $authorId;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->paymentDetails = $paymentDetails;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getPaymentDetails(): string
    {
        return $this->paymentDetails;
    }
}