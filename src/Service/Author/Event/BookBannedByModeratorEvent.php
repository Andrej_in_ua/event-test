<?php


namespace App\Service\Author\Event;


use App\Service\Event\AbstractEvent;

/**
 * Class BookBannedByModeratorEvent
 *
 * @package App\Service\Author\Event
 */
class BookBannedByModeratorEvent extends AbstractEvent
{
    /**
     * @var int
     */
    private $moderatorId;

    /**
     * @var int
     */
    private $authorId;

    /**
     * @var int
     */
    private $bookId;

    /**
     * @var string
     */
    private $moderatorComment;

    /**
     * BookBannedByModeratorEvent constructor.
     *
     * @param int $moderatorId
     * @param int $authorId
     * @param int $bookId
     * @param string $moderatorComment
     */
    public function __construct(int $moderatorId, int $authorId, int $bookId, string $moderatorComment)
    {
        $this->moderatorId = $moderatorId;
        $this->authorId = $authorId;
        $this->bookId = $bookId;
        $this->moderatorComment = $moderatorComment;
    }

    /**
     * @return int
     */
    public function getModeratorId(): int
    {
        return $this->moderatorId;
    }

    /**
     * @return int
     */
    public function getAuthorId(): int
    {
        return $this->authorId;
    }

    /**
     * @return int
     */
    public function getBookId(): int
    {
        return $this->bookId;
    }

    /**
     * @return string
     */
    public function getModeratorComment(): string
    {
        return $this->moderatorComment;
    }
}