<?php

namespace App\Service\Event;

/**
 * Class AbstractEvent
 */
abstract class AbstractEvent implements EventInterface
{
    /**
     * @inheritdoc
     */
    public static function getEventName(): string
    {
        if (static::class === self::class) {
            throw new LogicException('Can`t get event name without event');
        }

        return static::class;
    }
}