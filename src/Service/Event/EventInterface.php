<?php

namespace App\Service\Event;

interface EventInterface
{
    public static function getEventName(): string;
}