<?php

namespace App\Service\Event;


use App\Service\Database\Connection;

class EventDispatcher
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var callable[][]
     */
    private $listeners = [];

    /**
     * EventDispatcher constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $eventName
     * @param callable $callback
     * @return EventDispatcher
     */
    public function registerListener(string $eventName, callable $callback): EventDispatcher
    {
        $this->listeners[$eventName][] = $callback;
        return $this;
    }

    /**
     * @param EventInterface $event
     */
    public function fire(EventInterface $event): void
    {
        $this->logEvent($event);

        $eventName = $event::getEventName();
        if (isset($this->listeners[$eventName])) {
            foreach ($this->listeners[$eventName] as $callback) {
                $callback($event);
            }
        }
    }

    /**
     * Это очень упращенная версия работы с базой данных.
     * В таком виде это нельзя использовать в реальном проекте!
     * Сделано это причине отсутсвия требований к фреймворку или прослойке для работы с базой данных,
     * а реализовывать полноценное решение в рамках тестового задания не имеет смысла.
     *
     * @param EventInterface $event
     */
    private function logEvent(EventInterface $event): void
    {
        $stm = $this->connection->prepare(
            'INSERT INTO `event_log` (`event_name`, `ip`, `user_agent`, `event`, `created`)
            VALUES (:eventName, :ip, :userAgent, :event, :created)'
        );

        $clientIp = $_SERVER['REMOTE_ADDR'] ?? '127.0.0.1';
        $userAgent = $_SERVER['HTTP_USER_AGENT'] ?? null;

        $stm->bindValue(':eventName', $event::getEventName());
        $stm->bindValue(':ip', $clientIp);
        $stm->bindValue(':userAgent', $userAgent, $userAgent ? \PDO::PARAM_STR : \PDO::PARAM_NULL);
        $stm->bindValue(':event', serialize($event));
        $stm->bindValue(':created', date('Y-m-d H:i:s'));

        $stm->execute();
    }
}