<?php

namespace App\Service\Notification;


class DummyNotificationManager
{
    /**
     * @param int $recipient
     * @param string $notificationType
     * @param string $payload
     */
    public function sendNotification(int $recipient, string $notificationType, string $payload): void
    {
        var_dump([$recipient, $notificationType, $payload]);
    }
}