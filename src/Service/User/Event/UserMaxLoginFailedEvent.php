<?php


namespace App\Service\User\Event;


use App\Service\Event\AbstractEvent;

/**
 * Class UserMaxLoginFailedEvent
 *
 * @package App\Service\Users
 */
class UserMaxLoginFailedEvent extends AbstractEvent
{
    /**
     * @var int
     */
    private $userId;

    /**
     * UserMaxLoginFailedEvent constructor.
     *
     * @param int $userId
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}