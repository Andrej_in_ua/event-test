<?php

namespace App\EventListener;

use App\Service\Author\Event\AuthorRequestRoyaltyEvent;
use App\Service\Author\Event\BookBannedByModeratorEvent;
use App\Service\Event\EventDispatcher;
use App\Service\Notification\DummyNotificationManager;

class AuthorEventListener
{
    // Это сделано для примера, в рабочей системе это должнл настраиваться через конфиги
    private const ADMIN_ID  = 100500;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var DummyNotificationManager
     */
    private $notificationManager;

    public function __construct(EventDispatcher $eventDispatcher, DummyNotificationManager $notificationManager)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->notificationManager = $notificationManager;

        $this->eventDispatcher->registerListener(AuthorRequestRoyaltyEvent::getEventName(), [$this, 'onRequestRoyalty']);
        $this->eventDispatcher->registerListener(BookBannedByModeratorEvent::getEventName(), [$this, 'onBookBanned']);
    }

    /**
     * @param AuthorRequestRoyaltyEvent $event
     */
    public function onRequestRoyalty(AuthorRequestRoyaltyEvent $event): void
    {
        $this->notificationManager->sendNotification(self::ADMIN_ID, 'email', 'заявка принята');
        $this->notificationManager->sendNotification($event->getAuthorId(), 'email', 'заявка принята');
    }

    public function onBookBanned(BookBannedByModeratorEvent $event): void
    {
        $this->notificationManager->sendNotification(
            $event->getAuthorId(),
            'system',
            sprintf('Книга "%s" забанена по причине "%s"'. $event->getBookId(), $event->getModeratorComment())
        );
    }
}