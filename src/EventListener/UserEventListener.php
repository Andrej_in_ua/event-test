<?php

namespace App\EventListener;


use App\Service\Event\EventDispatcher;
use App\Service\Notification\DummyNotificationManager;
use App\Service\User\Event\UserMaxLoginFailedEvent;

class UserEventListener
{
    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var DummyNotificationManager
     */
    private $notificationManager;

    public function __construct(EventDispatcher $eventDispatcher, DummyNotificationManager $notificationManager)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->notificationManager = $notificationManager;

        $this->eventDispatcher->registerListener(UserMaxLoginFailedEvent::getEventName(), [$this, 'onMaxLoginFailed']);
    }

    public function onMaxLoginFailed(UserMaxLoginFailedEvent $event): void
    {
        $this->notificationManager->sendNotification(
            $event->getUserId(),
            'system',
            sprintf('Алярм, алярм! Много попыток входа! Император негодует!')
        );
    }
}